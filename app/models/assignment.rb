class Assignment < ApplicationRecord
  belongs_to :syllabus_entry
  before_save :default_values
  before_save :update_state
  validates :name, :due_date, :presence => true

  def default_values
    self.is_verified = false if self.is_verified.nil?
    self.number_of_verifications = 0 if self.number_of_verifications.nil?
  end

  def update_state
    self.name = self.name.strip unless self.name.nil?
    self.description = self.description.strip unless self.description.nil?
    if self.number_of_verifications >= 2 && !self.is_verified
      self.is_verified = true
      syllabus_entry = self.syllabus_entry
      unless syllabus_entry.are_assignments_verified
        syllabus_entry.update(verified_assignments_count: syllabus_entry.verified_assignments_count + 1)
      end
    end
  end
end
