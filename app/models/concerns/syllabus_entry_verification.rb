module SyllabusEntryVerification
  extend ActiveSupport::Concern

  included do

    def force_verify_entry
      self.course.update(is_verified: true)
      self.assignments.each do |assignment|
        assignment.update(is_verified: true)
      end
      self.is_course_verified = true
      self.are_assignments_verified = true
      self.is_verified = true
      self.verified_assignments_count = self.total_assignments_count
      self.save
      self.syllabus.update(is_verified: true)
    end

    def verify_entry
      verify_course
      verify_assignments
    end

    def verify_course
      course = self.course
      matches = matching_courses(course)
      matches.each {|c| c.update(number_of_verifications: c.number_of_verifications + 1)}
      if matches.count > 0
        course.update(number_of_verifications: course.number_of_verifications + matches.count)
      end
    end

    def matching_courses(course)
      syllabus = self.syllabus
      query = 'lower(name) = ? AND lower(number) = ? AND lower(section) = ? AND lower(term) = ? AND lower(instructor) = ?'
      syllabus.courses.where(query, course.name.downcase, course.number.downcase,
                                                   course.section.downcase, course.term.downcase,
                                                   course.instructor.downcase).where.not(id: course.id)
    end

    def verify_assignments
      assignments = self.assignments
      global_matched_assignments_ids = []
      assignments.each do |assignment|
        match_count = 0
        matched_syllabus_entry_ids = []
        matching_assignments(assignment).each do |a|
          a_id = a.id
          a_entry_id = a.syllabus_entry.id
          if !matched_syllabus_entry_ids.include?(a_entry_id) && !global_matched_assignments_ids.include?(a_id)
            a.update(number_of_verifications: a.number_of_verifications + 1)
            match_count += 1
            global_matched_assignments_ids.push(a_id)
            matched_syllabus_entry_ids.push(a_entry_id)
          end
        end

        if match_count > 0
          assignment.update(number_of_verifications: assignment.number_of_verifications + match_count)
        end
      end
    end

    def matching_assignments(assignment)
      syllabus = self.syllabus
      query = 'lower(name) = ? AND due_date = ? AND lower(description) = ? AND possible_points = ?'
      syllabus.assignments.where(query, assignment.name.downcase, assignment.due_date,
                                                        assignment.description.downcase, assignment.possible_points).
          where.not(syllabus_entry_id: assignment.syllabus_entry_id)
    end

  end
end
