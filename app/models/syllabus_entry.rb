class SyllabusEntry < ApplicationRecord
  include SyllabusEntryVerification
  belongs_to :syllabus
  has_one :course, dependent: :destroy
  has_many :assignments, dependent: :destroy
  validate :entry_count_validation, on: :create
  before_save :default_values
  before_save :update_state
  after_save :send_if_verified


  def default_values
    self.is_verified = false if self.is_verified.nil?
    self.is_course_verified = false if self.is_course_verified.nil?
    self.are_assignments_verified = false if self.are_assignments_verified.nil?
    self.verified_assignments_count = 0 if self.verified_assignments_count.nil?
    self.total_assignments_count = self.assignments.count
    true
  end

  def update_state
    if (self.total_assignments_count > 0) && (self.verified_assignments_count == self.total_assignments_count)
      self.are_assignments_verified = true
    end

    if self.is_course_verified && self.are_assignments_verified
      self.is_verified = true
      unless self.syllabus.is_verified
        self.syllabus.update(is_verified: true)
      end
    end
    true
  end

  def entry_count_validation
    if Syllabus.find(self.syllabus_id).syllabus_entries.count >= 10
      errors.add(:syllabus_id, "Syllabus already has enough entries")
    end
  end

  def add_assignments(assignments, is_admin)
    if self.total_assignments_count == 0
      assignments.each do |assignment|
        Assignment.create(syllabus_entry_id: self.id, name: assignment[:name], due_date: assignment[:due_date],
                          description: assignment[:description], possible_points: assignment[:possible_points])
      end
      self.total_assignments_count = assignments.length
    end

    if is_admin
      self.force_verify_entry
    else
      self.verify_entry
    end

    self
  end

  def send_if_verified
    if self.is_verified?
      send_course_to_students
      delete_all_existing_assignments
      self.assignments.each do |assignment|
        counter = 0
        AssignmentsService.new.post(
            payload: {
                name: params[:assignments][counter].name,
                due_date: params[:assignments][counter].due_date,
                possible_points: params[:assignments][counter].possible_points,
                description: params[:assignments][counter].description,
                course_id: model.syllabus.external_course_id
            }
        )
        counter += 1
      end
    end
  end

  def send_course_to_students
    CoursesService.new.put(
        id: self.syllabus.external_course_id,
        payload: {
            name: self.course.name,
            number: self.course.number,
            section: self.course.section,
            term: self.course.term,
            instructor: self.course.instructor,
            verified: self.course.is_verified?
        })
  end

  def send_assignment_to_students
    AssignmentsService.new.post(
        payload: {
            name: self.assignments[counter].name,
            due_date: self.assignments[counter].due_date,
            possible_points: self.assignments[counter].possible_points,
            description: self.assignments[counter].description,
            course_id: self.syllabus.external_course_id
        }
    )
  end

  def delete_all_existing_assignments
    AssignmentsService.new.destroy(self.course.syllabus_entry.syllabus.external_course_id)
  end

end
