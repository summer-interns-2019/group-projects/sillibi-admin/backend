class Syllabus < ApplicationRecord
  validates_uniqueness_of :external_course_id
  has_many :syllabus_entries, dependent: :destroy
  has_many :courses, through: :syllabus_entries
  has_many :assignments, through: :syllabus_entries

  before_save :default_values

  def default_values
    self.is_verified = false if self.is_verified.nil?
  end

end