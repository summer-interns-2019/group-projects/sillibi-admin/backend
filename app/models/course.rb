class Course < ApplicationRecord
  belongs_to :syllabus_entry
  validates :name, :number, :section, :instructor, :presence => true
  validate :course_existence_validation, on: :create
  before_save :default_values
  before_save :update_state

  def default_values
    self.is_verified = false if self.is_verified.nil?
    self.number_of_verifications = 0 if self.number_of_verifications.nil?
    true
  end

  def update_state
    self.name = self.name.strip unless self.name.nil?
    self.number = self.number.strip unless self.number.nil?
    self.section = self.section.strip unless self.section.nil?
    self.term = self.term.strip unless self.term.nil?
    self.instructor = self.instructor.strip unless self.instructor.nil?
    self.color = self.color.strip unless self.color.nil?
    if self.number_of_verifications >= 2 && !self.is_verified
      self.is_verified = true
      syllabus_entry = self.syllabus_entry
      unless syllabus_entry.is_course_verified
        syllabus_entry.update!(is_course_verified: true)
      end
    end
  end

  def course_existence_validation
    course = SyllabusEntry.find(self.syllabus_entry_id).course
    if course != nil && course.id != self.id
      errors.add(:syllabus_entry_id, "Syllabus Entry already has a course")
    end
  end

end
