class SyllabusFinder
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :whatever

  def persisted?
    false
  end

  def serve
    Syllabus.find(ordered_syllabuses.first)
  end

  def ordered_syllabuses
    order_syllabuses_by_entry_count_then_created_at_date
  end

  def order_syllabuses_by_entry_count_then_created_at_date
    if ordered_syllabuses_without_entries_ids.present?
      ordered_syllabuses_without_entries_ids
    else
      ordered_syllabuses_with_entries_ids
    end
  end

  private
    def ordered_syllabuses_without_entries_ids
      @ordered_syllabuses_without_entries_ids ||= Syllabus.includes(:syllabus_entries).
          where(syllabus_entries: { syllabus_id: nil }).order(:created_at).limit(1).ids
    end

    def ordered_syllabuses_with_entries_ids
      @ordered_syllabuses_with_entries_ids ||= SyllabusEntry.where(syllabus_id: Syllabus.where(is_verified: false)).
        where(syllabus_id: SyllabusEntry.group(:syllabus_id).having("count(*) < ?", 10).count.keys).
        group(:syllabus_id).order(Arel.sql("count(*)")).count.keys
    end

end
