class User < ApplicationRecord
  acts_as_token_authenticatable
  before_save :default_values
  def default_values
    self.admin = false if self.admin.nil?
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable, :jwt_authenticatable, :registerable,
         jwt_revocation_strategy: JWTBlacklist
end
