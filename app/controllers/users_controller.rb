class UsersController < ApplicationController

  #### REMOVE BEFORE PRODUCTION
  def index
    render json: User.all
  end

  def create
    user = User.create!(email: params[:email], password: params[:password])

    if user != nil
      render json: user.as_json(only: [:email, :authentication_token]), status: :created
      sign_in(:user, user)
    else
      head(:unauthorized)
    end
  end

  #### REMOVE BEFORE PRODUCTION
  def show
    render json: User.where(id: params[:id]).first
  end

  def destroy
  end
end