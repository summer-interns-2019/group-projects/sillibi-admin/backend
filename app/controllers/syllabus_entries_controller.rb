class SyllabusEntriesController < ActiveRecordApi::Rest::Controller
  before_action :authenticate_admin!, except: [:create, :put_assignments]

  def put_assignments
    render json: SyllabusEntry.find(params[:id]).add_assignments(params[:assignments], current_user.admin)
    syllabus_entry = SyllabusEntry.find(params[:id])
    if syllabus_entry.is_verified?
      send_course_to_students
      delete_all_existing_assignments
      syllabus_entry.assignments.each do |assignment|
        counter = 0
        AssignmentsService.new.post(
            payload: {
                name: params[:assignments][counter].name,
                due_date: params[:assignments][counter].due_date,
                possible_points: params[:assignments][counter].possible_points,
                description: params[:assignments][counter].description,
                course_id: model.syllabus.external_course_id
            }
        )
        counter += 1
      end
    end
  end

  def send_course_to_students
    CoursesService.new.put(
        id: model.syllabus.external_course_id,
        payload: {
            name: model.course.name,
            number: model.course.number,
            section: model.course.section,
            term: model.course.term,
            instructor: model.course.instructor,
            verified: model.course.is_verified?
        })
  end

  def send_assignment_to_students
    AssignmentsService.new.post(
        payload: {
            name: params[:assignments][counter].name,
            due_date: params[:assignments][counter].due_date,
            possible_points: params[:assignments][counter].possible_points,
            description: params[:assignments][counter].description,
            course_id: model.syllabus.external_course_id
        }
    )
  end

  def delete_all_existing_assignments
    AssignmentsService.new.destroy(model.syllabus.external_course_id)
  end

  protected
      def validate_params
        return unless not_allowed_params.present? && !(not_allowed_params.length == 1 && not_allowed_params[0] == :assignments)
        render json: { base: "Extra parameters are not allow: #{not_allowed_params.join(', ')}" }, status: :unprocessable_entity
      end

  private
    def authenticate_admin!
      return unless !current_user.admin
      render json: {base: "You need Admin privileges to perform this action"}, status: :unauthorized
    end
end
