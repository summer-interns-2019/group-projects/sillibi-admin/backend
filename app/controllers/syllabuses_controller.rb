class SyllabusesController < ActiveRecordApi::Rest::Controller
  before_action :authenticate_admin!, except: [:serve, :is_admin]

  def serve
    render json: SyllabusFinder.new.serve
  end

  def is_admin
    render json: {is_admin: current_user.admin}
  end

  private

    def authenticate_admin!
      return unless !current_user.admin
      render json: {base: "You need Admin privileges to perform this action"}, status: :unauthorized
    end
end
