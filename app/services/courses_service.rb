class CoursesService < ActiveRecordApi::Request::Methods
  def host
    'http://sillibi.herokuapp.com'
  end

  def path
    '/courses'
  end

  def credentials
    {
        user: {
            email: Rails.application.credentials.user[:email],
            password: Rails.application.credentials.user[:password]
        }
    }
  end

  def token_path
    'login'
  end

  def debug
    true
  end
end
