class SyllabusEntrySerializer < ActiveModel::Serializer
  has_one :course
  has_many :assignments
  attributes :id
end
