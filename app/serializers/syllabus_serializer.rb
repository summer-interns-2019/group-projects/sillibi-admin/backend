class SyllabusSerializer < ActiveModel::Serializer
  has_many :syllabus_entries
  attributes :id, :data, :is_verified, :flagged, :external_course_id
end
