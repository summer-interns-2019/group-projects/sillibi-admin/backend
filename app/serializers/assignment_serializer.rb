class AssignmentSerializer < ActiveModel::Serializer
  attributes :id, :name, :due_date, :description, :possible_points
end
