class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :number, :section, :term, :instructor
end
