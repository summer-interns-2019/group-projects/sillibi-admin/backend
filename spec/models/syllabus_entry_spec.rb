require "rails_helper"

RSpec.describe SyllabusEntry, :type => :model do
  describe "#verify_entry" do
    context "Verifiable Syllabus" do
      let(:syllabus){ create :syllabus }
      let!(:syllabus_entry0){ create :syllabus_entry, :uniform0, syllabus: syllabus }
      let!(:syllabus_entry1){ create :syllabus_entry, :uniform0, syllabus: syllabus }
      let(:syllabus_entry2){ create :syllabus_entry, :uniform0, syllabus: syllabus }
      before(:each) do
        syllabus_entry0.verify_entry
        syllabus_entry1.verify_entry
        syllabus_entry2.verify_entry
      end
      it { expect(syllabus.reload.is_verified).to be true }
    end

    context "Verify where Assignment verified by single assignment in other syllabuses" do
      let(:syllabus){ create :syllabus }
      let!(:syllabus_entry0){ create :syllabus_entry, :rainbow0, syllabus: syllabus }
      let!(:syllabus_entry1){ create :syllabus_entry, :rainbow1, syllabus: syllabus }
      let!(:syllabus_entry2){ create :syllabus_entry, :rainbow2, syllabus: syllabus }
      let!(:syllabus_entry3){ create :syllabus_entry, :rainbow3, syllabus: syllabus }
      before(:each) do
        syllabus_entry0.verify_entry
      end
      it { expect(syllabus.reload.is_verified).to be true }
    end

  end
end