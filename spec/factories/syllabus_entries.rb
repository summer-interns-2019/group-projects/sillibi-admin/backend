FactoryBot.define do
  factory :syllabus_entry do
    syllabus

    11.times do |n|
      trait :"uniform#{n}" do
        transient do
          assignment_count { 5 }
        end
        after(:create) do |syllabus_entry, evaluator|
          create_list(:assignment, evaluator.assignment_count, :"consistent#{n}", syllabus_entry: syllabus_entry)
          syllabus_entry.update(total_assignments_count: 5)
          create(:course, :"consistent#{n}", syllabus_entry: syllabus_entry)
        end
      end
    end

    trait :rainbow0 do
      after(:create) do |syllabus_entry, evaluator|
        (0..2).each do |i|
          create(:assignment, :"consistent#{i}", syllabus_entry: syllabus_entry)
        end
        syllabus_entry.update(total_assignments_count: 3)
        create(:course, :consistent0, syllabus_entry: syllabus_entry)
      end
    end

    trait :rainbow1 do
      after(:create) do |syllabus_entry, evaluator|
        create(:assignment, :consistent3, syllabus_entry: syllabus_entry)
        create(:assignment, :consistent1, syllabus_entry: syllabus_entry)
        create(:assignment, :consistent0, syllabus_entry: syllabus_entry)
        syllabus_entry.update(total_assignments_count: 3)
        create(:course, :consistent0, syllabus_entry: syllabus_entry)
      end
    end

    trait :rainbow2 do
      after(:create) do |syllabus_entry, evaluator|
        create(:assignment, :consistent5, syllabus_entry: syllabus_entry)
        create(:assignment, :consistent0, syllabus_entry: syllabus_entry)
        create(:assignment, :consistent2, syllabus_entry: syllabus_entry)
        syllabus_entry.update(total_assignments_count: 3)
        create(:course, :consistent0, syllabus_entry: syllabus_entry)
      end
    end

    trait :rainbow3 do
      after(:create) do |syllabus_entry, evaluator|
        create(:assignment, :consistent2, syllabus_entry: syllabus_entry)
        create(:assignment, :consistent4, syllabus_entry: syllabus_entry)
        create(:assignment, :consistent1, syllabus_entry: syllabus_entry)
        syllabus_entry.update(total_assignments_count: 3)
        create(:course, :consistent2, syllabus_entry: syllabus_entry)
      end
    end



  end
end
