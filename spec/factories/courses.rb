FactoryBot.define do
  factory :course do
    name { Faker::Name.name }
    number { Faker::Number.number(digits:3)  }
    section { Faker::Number.number(digits:2) }
    term { %w(Spring Fall).sample }
    instructor { Faker::Name.name }
    color { nil }
    syllabus_entry

    10.times do |n|
      trait :"consistent#{n}" do
        name { "Business Administration #{n}" }
        number { "MGMT 100 #{n}" }
        section { "001 #{n}" }
        term { "Fall 2017 #{n}" }
        instructor { "Dr. Sam Birk #{n}" }
      end
    end

  end
end
