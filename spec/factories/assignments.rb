FactoryBot.define do
  factory :assignment do
    name { Faker::Name.name }
    due_date { 5.days.from_now }
    description { Faker::Lorem.sentence }
    possible_points { 10 }
    syllabus_entry

    10.times do |n|
      trait :"consistent#{n}" do
        name { "Assignment Something #{n}" }
        description { "Some Description #{n}" }
      end
    end

  end
end
