FactoryBot.define do
  factory :syllabus do
    external_course_id { Faker::Number.number(digits:9) }
    data { Faker::ChuckNorris.fact }
  end
end
