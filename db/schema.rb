# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_02_143354) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.string "name"
    t.date "due_date"
    t.text "description"
    t.integer "possible_points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "number_of_verifications"
    t.boolean "is_verified"
    t.bigint "syllabus_entry_id"
    t.index ["syllabus_entry_id"], name: "index_assignments_on_syllabus_entry_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.string "number"
    t.string "section"
    t.string "term"
    t.string "instructor"
    t.string "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "number_of_verifications"
    t.boolean "is_verified"
    t.bigint "syllabus_entry_id"
    t.index ["syllabus_entry_id"], name: "index_courses_on_syllabus_entry_id"
  end

  create_table "jwt_blacklist", force: :cascade do |t|
    t.string "jti", null: false
    t.datetime "exp", null: false
    t.index ["jti"], name: "index_jwt_blacklist_on_jti"
  end

  create_table "syllabus_entries", force: :cascade do |t|
    t.boolean "is_course_verified"
    t.integer "verified_assignments_count"
    t.integer "total_assignments_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "syllabus_id"
    t.boolean "are_assignments_verified"
    t.boolean "is_verified"
    t.index ["syllabus_id"], name: "index_syllabus_entries_on_syllabus_id"
  end

  create_table "syllabuses", force: :cascade do |t|
    t.integer "external_course_id"
    t.text "data"
    t.boolean "flagged"
    t.text "data", limit: 16777215
    t.boolean "is_verified"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "flagged"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token", limit: 30
    t.boolean "admin"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
