class AddSyllabusEntryReferenceToCourse < ActiveRecord::Migration[5.2]
  def change
    add_reference :courses, :syllabus_entry, index: true
  end
end
