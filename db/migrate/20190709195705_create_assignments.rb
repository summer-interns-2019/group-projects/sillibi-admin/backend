class CreateAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :assignments do |t|
      t.string :name
      t.date :due_date
      t.text :description
      t.integer :possible_points
      t.integer :course_id

      t.timestamps
    end
  end
end
