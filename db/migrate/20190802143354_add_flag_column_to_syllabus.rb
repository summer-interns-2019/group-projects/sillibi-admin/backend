class AddFlagColumnToSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabuses, :flagged, :boolean
  end
end
