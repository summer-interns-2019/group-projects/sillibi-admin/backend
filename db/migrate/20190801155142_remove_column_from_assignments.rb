class RemoveColumnFromAssignments < ActiveRecord::Migration[5.2]
  def change
    remove_column :assignments, :course_id
  end
end
