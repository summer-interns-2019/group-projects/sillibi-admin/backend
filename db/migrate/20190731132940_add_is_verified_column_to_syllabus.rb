class AddIsVerifiedColumnToSyllabus < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabuses, :is_verified, :boolean
  end
end
