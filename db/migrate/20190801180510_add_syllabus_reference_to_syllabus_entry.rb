class AddSyllabusReferenceToSyllabusEntry < ActiveRecord::Migration[5.2]
  def change
    add_reference :syllabus_entries, :syllabus, index: true
  end
end
