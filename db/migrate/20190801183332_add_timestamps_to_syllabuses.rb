class AddTimestampsToSyllabuses < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabuses, :created_at, :datetime
    add_column :syllabuses, :updated_at, :datetime
  end
end
