class ChangeSyllabusCourseIdColumnNameToExternalCourseId < ActiveRecord::Migration[5.2]
  def change
    rename_column :syllabuses, :course_id, :external_course_id
  end
end
