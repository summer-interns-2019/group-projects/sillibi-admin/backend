class AddSyllabusEntryReferenceToAssignments < ActiveRecord::Migration[5.2]
  def change
    add_reference :assignments, :syllabus_entry, index: true
  end
end
