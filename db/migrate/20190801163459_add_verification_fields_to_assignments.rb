class AddVerificationFieldsToAssignments < ActiveRecord::Migration[5.2]
  def change
    add_column :assignments, :number_of_verifications, :integer
    add_column :assignments, :is_assignment_verified, :boolean
  end
end
