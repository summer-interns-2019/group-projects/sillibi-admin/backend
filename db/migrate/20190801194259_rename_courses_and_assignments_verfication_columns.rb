class RenameCoursesAndAssignmentsVerficationColumns < ActiveRecord::Migration[5.2]
  def change
    rename_column :courses, :is_course_verified, :is_verified
    rename_column :assignments, :is_assignment_verified, :is_verified
  end
end
