class AddAssignmentVerficationColumnToSyllabusEntry < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_entries, :are_assignments_verified, :boolean
  end
end
