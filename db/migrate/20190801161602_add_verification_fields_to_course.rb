class AddVerificationFieldsToCourse < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :number_of_verifications, :integer
    add_column :courses, :is_course_verified, :boolean
  end
end
