class AddSyllabusEntriesTable < ActiveRecord::Migration[5.2]
  def change
    create_table :syllabus_entries do |t|
      t.boolean :is_course_verified
      t.integer :verified_assignments_count
      t.integer :total_assignments_count
      t.timestamps
    end
  end
end
