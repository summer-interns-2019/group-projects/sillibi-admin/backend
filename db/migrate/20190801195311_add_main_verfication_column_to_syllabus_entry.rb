class AddMainVerficationColumnToSyllabusEntry < ActiveRecord::Migration[5.2]
  def change
    add_column :syllabus_entries, :is_verified, :boolean
  end
end
