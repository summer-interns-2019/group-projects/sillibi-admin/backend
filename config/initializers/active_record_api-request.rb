require 'active_record_api/request'

module ActiveRecordApi
  module Request
    class TokenRetriever < Methods
      def fetch_token
        response = authenticated_connection.post(full_url, credentials)
        response.headers[:authorization]&.split&.last
      end
    end
  end
end
