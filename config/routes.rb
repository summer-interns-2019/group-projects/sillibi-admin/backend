Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # resources :assignments
  resources :courses, only: [:create, :update]
  resources :syllabuses do
    collection do
      get :serve
      get :is_admin
    end
  end

  resources :syllabus_entries do
    member do
      put :put_assignments
    end
  end
  resources :assignments

  devise_for :users,
             path: '',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout',
                 registration: 'signup'
             },
             controllers: {
                 sessions: 'sessions',
                 registrations: 'registrations'
             }
end
